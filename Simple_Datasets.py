import torch
from torch.utils.data import Dataset

# forces the random function to give the same number every time we recompile it
torch.manual_seed(1)


# %%
# Define class for dataset
class toy_set(Dataset):

    # Constructor with default values
    def __init__(self, length=100, transform=None):
        self.len = length
        self.transform = transform

        # length is the number of rows. Multiplies everything in it by 2
        self.x = 2 * torch.ones(length, 2)
        self.y = torch.ones(length, 1)

    # Getter
    # Usually used for list indexing, dictionary lookup, or accessing ranges of values
    def __getitem__(self, index):
        sample = self.x[index], self.y[index]
        if self.transform:
            sample = self.transform(sample)

        return sample

    # Get length
    def __len__(self):
        return self.len


our_dataset = toy_set()
print(our_dataset)

# When you call index as [i], it calls obj.__getitem__(i)
print(our_dataset[0])
print(our_dataset.__len__())

# print out first 3 elements and assign them to x and y
for i in range(3):
    x, y = our_dataset[i]

for x, y in our_dataset:
    print('x: {}, y: {}'.format(x, y))

# %%


"""Transforms"""


# %%
# Create transform class add_mult

class add_mult(object):

    # Constructor
    def __init__(self, addx=1, muly=2):
        self.addx = addx
        self.muly = muly

    # Executor
    # instances behave like functions and can be called like a function
    def __call__(self, sample):
        # Gets the x and y from sample (which returns x, y from the specified index)
        x = sample[0]
        y = sample[1]
        x += self.addx
        y *= self.muly
        sample = x, y
        return sample


# Create transform object
a_m = add_mult()
data_set = toy_set()

# Use loop to print out first 10 elements in dataset
for i in range(10):
    x, y = data_set[i]
    print('Index: ', i, 'Original x: ', x, 'Original y: ', y)
    x_, y_ = a_m(data_set[i])
    print('Index: ', i, 'Transformed x_:', x_, 'Transformed y_:', y_)

# transform is a function because of __call__().
# dont' need to put brackets
cust_data_set = toy_set(transform=a_m)
# %%


"""Compose"""
# %%
from torchvision import transforms


# Create tranform class mult

class mult(object):

    # Constructor
    def __init__(self, mult=100):
        self.mult = mult

    # Executor
    def __call__(self, sample):
        x = sample[0]
        y = sample[1]
        x = x * self.mult
        y = y * self.mult
        sample = x, y
        return sample


# Combine the add_mult() and mult()

data_transform = transforms.Compose([add_mult(), mult()])
print("The combination of transforms (Compose): ", data_transform)

data_transform(data_set[0])
x, y = data_set[0]
x_, y_ = data_transform(data_set[0])
print('Original x: ', x, 'Original y: ', y)

print('Transformed x_:', x_, 'Transformed y_:', y_)

# Create a new toy_set object with compose object as transform
compose_data_set = toy_set(transform=data_transform)

# Use loop to print out first 3 elements in dataset
for i in range(3):
    x, y = data_set[i]
    print('Index: ', i, 'Original x: ', x, 'Original y: ', y)
    x_, y_ = cust_data_set[i]
    print('Index: ', i, 'Transformed x_:', x_, 'Transformed y_:', y_)
    x_co, y_co = compose_data_set[i]
    print('Index: ', i, 'Compose Transformed x_co: ', x_co, 'Compose Transformed y_co: ', y_co)
# %%


"""Practice"""
# Practice: Make a compose as mult() execute first and then add_mult(). Apply the compose on toy_set dataset.
# Print out the first 3 elements in the transformed dataset.
# %%

# what it does to both the x and the y in order.
# Can have multiple args.
my_compose = transforms.Compose([add_mult(), mult()])
my_transformed_dataset = toy_set(transform=my_compose)
for i in range(3):
    x_, y_ = my_transformed_dataset[i]
    print('Index: ', i, 'Transformed x_:', x_, 'Transformed y_:', y_)
# %%


"""IDEK"""
# %%
# %%
