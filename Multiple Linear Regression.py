import matplotlib.pyplot as plt
import pandas as pd
import pylab as pl
import numpy as np

pd.set_option('display.width', 400)
pd.set_option('display.max_columns', 15)

df = pd.read_csv('CSVs/FuelConsumptionCo2.csv')
cdf = df[
    ['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_CITY', 'FUELCONSUMPTION_HWY', 'FUELCONSUMPTION_COMB', 'CO2EMISSIONS']]

# Plot emission value with respect to engine size
plt.scatter(cdf['CO2EMISSIONS'], cdf['ENGINESIZE'], color='blue')
plt.xlabel('Engine Size')
plt.ylabel('Emissions')
plt.show()

"""Create training and testing data"""
# %%
msk = np.random.randn(df.__len__()) < 0.8
train = cdf[msk]
test = cdf[~msk]
# %%


"""Train data distribution"""
# %%
# Plot with masked training data
plt.scatter(train.ENGINESIZE, train.CO2EMISSIONS, color='blue')
plt.xlabel('Engine size')
plt.ylabel('Emission')
plt.show()
# %%


"""Multiple regression model"""
# %%
from sklearn import linear_model

regr = linear_model.LinearRegression()
# Many x values because you're doing multiple regression
x = np.asanyarray(train[['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_COMB']])
y = np.asanyarray(train[['CO2EMISSIONS']])
regr.fit(x, y)
# the coefficients. They are the parameters of the fitted line. Sklearn estimates it from data
# OLS tries to minimize the sum of squared errors or mean squared error (MSE) between target variable
# and predicted output
print('Coefficients: ', regr.coef_)
# %%


"""Prediction"""
# %%
# tries to predict the y value of the x using those 3 data points
# y_hat is the estimated target output and y is the correct target output
y_hat = regr.predict(test[['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_COMB']])
x = np.asanyarray(test[['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_COMB']])
y = np.asanyarray(test[['CO2EMISSIONS']])

# .2 means to only print the first two digits after the decimal point. f means float.
print("Residual sum of squares: %.2f" % np.mean((y_hat - y) ** 2))

# Explained variance score: 1 is perfect prediction
# score returns the coefficient of determination r^2 of the prediction
print('Variance score: %.2f' % regr.score(x, y))

# %%


"""Practice"""
# %%
regr = linear_model.LinearRegression()
x = np.asanyarray(test[['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_CITY', 'FUELCONSUMPTION_HWY']])
y = np.asanyarray(test[['CO2EMISSIONS']])
regr.fit(x, y)

y_hat = regr.predict(test[['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_CITY', 'FUELCONSUMPTION_HWY']])
x = np.asanyarray(test[['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_CITY', 'FUELCONSUMPTION_HWY']])
y = np.asanyarray(test[['CO2EMISSIONS']])

print("Residual sum of squares: %.2f" % np.mean((y_hat - y) ** 2))
print('Variance score: %.2f' % regr.score(x, y))
# %%
