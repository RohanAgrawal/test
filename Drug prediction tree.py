import numpy as np
import pandas as pd
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt

# Decision tree tries to predict what drug to give to the patient

df = pd.read_csv('CSVs/drug200.csv')

"""Pre-processing"""
# Transforms the strings to ints
# %%
from sklearn import preprocessing

# Set X as the Feature Matrix (data of df)
# Set y as the response vector (target)
X = df[['Age', 'Sex', 'BP', 'Cholesterol', 'Na_to_K']].values
le_sex = preprocessing.LabelEncoder()
le_sex.fit(['F', 'M'])

# Transform transforms labels to normalized encoding
X[:, 1] = le_sex.transform(X[:, 1])

le_BP = preprocessing.LabelEncoder()
le_BP.fit(['LOW', 'NORMAL', 'HIGH'])
X[:, 2] = le_BP.transform(X[:, 2])

le_Chol = preprocessing.LabelEncoder()
le_Chol.fit(['NORMAL', 'HIGH'])
X[:, 3] = le_Chol.transform(X[:, 3])

# target variable (variable to find out)
y = df['Drug']
# %%


"""Setting up the decision tree"""
# %%
from sklearn.model_selection import train_test_split

X_trainset, X_testset, y_trainset, y_testset = train_test_split(X, y, test_size=0.3, random_state=3)
# X and y trainsets and X and y testsets should be the same shape
# %%


"""Modeling"""
# %%
drugTree = DecisionTreeClassifier(criterion='entropy', max_depth=4)
drugTree.fit(X_trainset, y_trainset)
# %%


"""Prediction"""
# %%
# predicted y values
predTree = drugTree.predict(X_testset)
print(predTree[0:5])
print(y_testset[0:5])
# %%


"""Evaluation"""
# %%
from sklearn import metrics

# tests predicted y values
print("DecisionTrees's Accuracy: ", metrics.accuracy_score(y_testset, predTree))
# %%


"""Visualization"""
# %%
from io import StringIO
import pydotplus
import matplotlib.image as mpimg
from sklearn import tree

dot_data = StringIO()
filename = "drugtree.png"
featureNames = df.columns[0:5]
out = tree.export_graphviz(drugTree, feature_names=featureNames, out_file=dot_data, class_names=np.unique(y_trainset),
                           filled=True, special_characters=True, rotate=False)
graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
graph.write_png(filename)
img = mpimg.imread(filename)
plt.figure(figsize=(100, 200))
plt.imshow(img, interpolation='nearest')
# %%
