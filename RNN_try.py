import numpy as np
import random
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
import time
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.callbacks import EarlyStopping
from keras.models import load_model

# %%
# %%
# Try 50 trials
repeats = 1
# timesteps = 200
timesteps = 2000

random.seed(1)
different_arrays = None
final_array = None
final_array_final = None

for f in range(repeats):
    # starting_value = random.randint(0, 360)
    starting_value = 180
    final_array = np.array([])

    i = 0
    while i < timesteps:
        number = random.randint(-5, 5)
        final_array = np.append(final_array, number)
        i += 1

    values = np.array([starting_value])

    for x in final_array:
        if x + starting_value > 360:
            starting_value = 0
        elif x + starting_value < 0:
            starting_value = 360
        starting_value += x
        values = np.append(values, starting_value)

    values = values[:-1]

    # sets the shape for different_arrays array
    if different_arrays is None:
        different_arrays = np.zeros([repeats, values.__len__()])

    if final_array_final is None:
        final_array_final = np.zeros([repeats, final_array.__len__()])

    # OUTPUTS
    different_arrays[f] = values
    # INPUTS
    final_array_final[f] = final_array
# %%

# %%

# TODO: MAKE AN LSTM/RNN FROM THESE INPUTS AND OUTPUTS

# %%
### Use inputs to predict target
# Scale features
s1 = MinMaxScaler(feature_range=(-1, 1))

inputs = final_array_final
outputs = different_arrays

# TODO: make multiple data frame columns

# Reshape it so it's (10000, 1)
inputs = np.reshape(inputs, (inputs.shape[1], 1))
outputs = np.reshape(outputs, (outputs.shape[1], 1))

# Will be 7k values (10k total)
train = inputs
# This thing's shape needs to be (10000, 1)
predicted = outputs
# # will also be 7k values out of 10k total
# train2 = starting_value[:7000]

# Train the data from the first 7000 rows.
# added both train and train2 here
Xs = s1.fit_transform(train)

# scale predicted value
s2 = MinMaxScaler(feature_range=(-1, 1))
# predictedFinal = np.reshape(predicted, (-1, 1))
# Ys = s2.fit_transform(predictedFinal)
Ys = s2.fit_transform(predicted)

# Each time step uses last 'window' to predict the next change
window = 70
X = []
Y = []
for i in range(window, len(Xs)):
    # x is input, y is output
    X.append(Xs[i - window:i, :])
    Y.append(Ys[i])

# Reshape data to format accepted by LSTM
X, Y = np.array(X), np.array(Y)

# create and train LSTM model
model = Sequential()
model.add(LSTM(units=50, return_sequences=True, input_shape=(X.shape[1], X.shape[2])))
model.add(Dropout(0.2))
model.add(LSTM(units=50, return_sequences=True))
model.add(Dropout(0.2))
model.add(LSTM(units=50))
model.add(Dropout(0.2))
model.add(Dense(units=1))
model.compile(optimizer='adam', loss='mean_squared_error', metrics=['accuracy'])

# Allow for early exit
es = EarlyStopping(monitor='loss', mode='min', verbose=1, patience=10)

# Fit (and time) LSTM model
t0 = time.time()
history = model.fit(X, Y, epochs=50, batch_size=250, callbacks=[es])

t1 = time.time()
print('Runtime: %.2f s' % (t1 - t0))
# %%

# Plotting
# %%
plt.figure(figsize=(8, 4))
plt.semilogy(history.history['loss'])
plt.xlabel('epoch')
plt.ylabel('loss')
model.save('model.h5')
plt.show()

# verify fit of the model
Yp = model.predict(X)

# un-scale outputs
Yu = s2.inverse_transform(Yp)
Ym = s2.inverse_transform(Y)

plt.figure(figsize=(10, 6))
# plt.plot(predicted[window:], Yu, 'r-', label='LSTM')
# plt.plot(predicted[window:], Yu, 'r-', label='LSTM')
Yu = np.reshape(Yu, (1, Yu.shape[0]))
Ym = np.reshape(Ym, (1, Ym.shape[0]))
plt.plot(Ym[0][:200], 'k-', label='Measured')
plt.plot(Yu[0][:200], 'r--', label='LSTM')
plt.title("Angle of Mouse")
plt.ylabel('Angle in Degrees')
plt.xlabel('Timesteps')
plt.legend()
plt.show()
# %%
