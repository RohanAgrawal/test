import os
import pandas as pd
from collections import Counter
import matplotlib.pyplot as plt


def count_words_fast(text):
    """
    Count the number of times each word occurs in text (str). Return dictionary
    where keys are unique words and values are word counts. Skip punctuation.
    """
    text = text.lower()
    skips = [".", ",", ";", ":", "'", '"']
    for ch in skips:
        text = text.replace(ch, "")

    # makes it into a dictionary with word and number of times it is shown
    word_counts = Counter(text.split(" "))

    return word_counts


def word_stats(word_counts):
    num_unique = len(word_counts)
    counts = word_counts.values()
    return (num_unique, counts)


# opens up the file and replaces all \n and \r in the text
hamlets = pd.read_csv("https://courses.edx.org/asset-v1:HarvardX+PH526x+2T2019+type@asset+block@hamlets.csv",
                      index_col=0)
language, text = hamlets.iloc[0]

# Find the dictionary of word frequency in text by calling count_words_fast(). Store this as counted_text.
# this is the dictionary
counted_text = count_words_fast(text)
counted_text = {k.replace("\n", ""): v for (k, v) in counted_text.items()}

# Pandas dataframe
# length is the length of each word
# frequency is based on events
data = pd.DataFrame(columns=("word", "count", "length", "frequency"))

# groups by frequency and counts each
data.groupby('frequency').count()


# from last example
def checkFrequency(count):
    count = int(count)
    if count > 10:
        return "frequent"
    elif 1 < count <= 10:
        return "infrequent"
    elif count == 1:
        return "unique"


data = pd.DataFrame({
    "word": list(counted_text.keys()),
    "count": list(counted_text.values()),
    "length": len(list(counted_text.keys())),
    "frequency": [checkFrequency(x) for x in list(counted_text.values())]

})


# example at the end
# data["length"] = data["word"].apply(len)
#
# data.loc[data["count"] > 10,  "frequency"] = "frequent"
# data.loc[data["count"] <= 10, "frequency"] = "infrequent"
# data.loc[data["count"] == 1,  "frequency"] = "unique"
#
# data.groupby('frequency').count()


# Exercise 5


def summarize_text(language, text):
    counted_text = count_words_fast(text)

    data = pd.DataFrame({
        "word": list(counted_text.keys()),
        "count": list(counted_text.values())
    })

    # checks count and then edits frequency
    data.loc[data["count"] > 10, "frequency"] = "frequent"
    data.loc[data["count"] <= 10, "frequency"] = "infrequent"
    data.loc[data["count"] == 1, "frequency"] = "unique"

    # data[word] is all the keys of the dictionary (all the words). Apply function applies a function
    # that function is the len() function. Don't need to put () because the apply() method already puts each word
    # in the parenthesis.
    data["length"] = data["word"].apply(len)

    sub_data = pd.DataFrame({
        "language": language,
        "frequency": ["frequent", "infrequent", "unique"],
        "mean_word_length": data.groupby(by="frequency")["length"].mean(),
        "num_words": data.groupby(by="frequency").size()
    })

    return (sub_data)


# gets the language and the text for that language
english, englishText = hamlets.iloc[0]
german, germanText = hamlets.iloc[1]
portuguese, portugueseText = hamlets.iloc[2]

grouped_data = pd.DataFrame(columns=("language", "frequency", "mean_word_length", "num_words"))
for i in range(3):
    language, text = hamlets.iloc[i]
    sub_data = summarize_text(language, text)
    grouped_data = grouped_data.append(sub_data)

# plots data
colors = {"Portuguese": "green", "English": "blue", "German": "red"}
markers = {"frequent": "o", "infrequent": "s", "unique": "^"}
for i in range(grouped_data.shape[0]):
    row = grouped_data.iloc[i]
    plt.plot(row.mean_word_length, row.num_words,
             marker=markers[row.frequency],
             color=colors[row.language],
             markersize=10
             )

color_legend = []
marker_legend = []
for color in colors:
    color_legend.append(
        plt.plot([], [],
                 color=colors[color],
                 marker="o",
                 label=color, markersize=10, linestyle="None")
    )
for marker in markers:
    marker_legend.append(
        plt.plot([], [],
                 color="k",
                 marker=markers[marker],
                 label=marker, markersize=5, linestyle="None")
    )
plt.legend(numpoints=1, loc="upper left")

plt.xlabel("Mean Word Length")
plt.ylabel("Number of Words")
plt.show()