import torch

"""Prediction"""
# %%
w = torch.tensor(2.0, requires_grad=True)
b = torch.tensor(-1.0, requires_grad=True)


# Function forward(x) for prediction
def forward(x):
    yhat = w * x + b
    return yhat


# Predicts y = 2x - 1 at x = 1
x = torch.tensor([[1.0]])
yhat = forward(x)
print("The predictions: ", yhat)

# Create x Tensor and check the shape of x tensor
x = torch.tensor([[1.0], [2.0]])
print("The shape of x: ", x.shape)

# Make the prediction of y = 2x - 1 at x = [1, 2]
# Prints the answer to both values of x
yhat = forward(x)
print("The prediction: ", yhat)
# %%


"""Class Linear"""
# %%
# Import Class Linear
from torch.nn import Linear

# set random seed
torch.manual_seed(1)

# Create linear regression model, print out the parameters
# in_features = size of each input sample
# out_features = size of each output sample
# Calculates the linear equation Ax = b where x is the input, b is the output, and A is the weight
lr = Linear(in_features=1, out_features=1, bias=True)
print("Parameters w and b: ", list(lr.parameters()))

# state_dict() returns a python dictionary object corresponding to layers of each parameter tensor
print("Python dictionary: ", lr.state_dict())
print("keys: ", lr.state_dict().keys())
print("values: ", lr.state_dict().values())

# Keys correspond to the name of the attribute and the values correspond to the parameter value
print("weight:", lr.weight)
print("bias:", lr.bias)

# Make single prediction at x = [[1.0]].
x = torch.tensor([[1.0]])
yhat = lr(x)
print("The prediction: ", yhat)

# Create the prediction using a linear model
x = torch.tensor([[1.0], [2.0]])
yhat = lr(x)
print("The prediction: ", yhat)
# %%


"""Build Custom Modules"""
# %%
from torch import nn


# Customize linear regression class
class LR(nn.Module):

    # Constructor
    def __init__(self, input_size, output_size):
        # Inherit from parent
        super().__init__()
        self.linear = nn.Linear(input_size, output_size)

    # Prediction function
    def forward(self, x):
        out = self.linear(x)
        return out


# Create the linear regression model. Print out the parameters.
lr = LR(1, 1)
print("The parameters: ", list(lr.parameters()))
print("Linear model: ", lr.linear)

print("*" * 40)
# Try our customize linear regression model with single input
x = torch.tensor([[1.0]])
yhat = lr(x)
print("The prediction: ", yhat)
# %%

