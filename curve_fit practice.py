import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# %%
x = np.arange(-5.0, 5.0, 0.1)

# Can adjust the slope and intercept to verify the changes in the graph
y = 2 * x + 3
# Noise just means that the data is spread out compared to the line.
y_noise = 2 * np.random.normal(size=x.size)
ydata = y + y_noise

plt.figure(figsize=(8, 6))
plt.plot(x, ydata, 'bo')
plt.plot(x, y, 'r')
plt.ylabel("Dependent Variable")
plt.xlabel("Independent Variable")
plt.show()
# %%


# Non linear regression is any relationship that is not linear
# ax^3 + bx^2 + cx + d
"""Cubic function graph"""
# %%
x = np.arange(-5.0, 5.0, 0.1)

# Can adjust the slope and intercept to verify the changes in the graph
y = 1 * (x ** 3) + 1 * (x ** 2) + 1 * x + 3
y_noise = 20 * np.random.normal(size=x.__len__())
ydata = y + y_noise
plt.plot(x, ydata, 'bo')
plt.plot(x, y, 'r')
plt.ylabel('Dependent Variable')
plt.xlabel('Independent Variable')
plt.show()

# %%


"""Quadratic"""
# %%
x = np.arange(-5.0, 5.0, 0.1)

# squares every element in x array
y = np.power(x, 2)
y_noise = 2 * np.random.normal(size=x.__len__())
ydata = y + y_noise
plt.plot(x, ydata, 'bo')
plt.plot(x, y, 'r')
plt.xlabel('Independent Variable')
plt.ylabel('Dependent Variable')
plt.show()
# %%


"""Exponential"""
# %%
x = np.arange(-5.0, 5.0, 0.1)
# Calculates the exponential of all elements in the array. It does e^x
y = np.exp(x)
y_noise = 20 * np.random.normal(size=x.__len__())
ydata = y_noise + y

plt.plot(x, ydata, 'bo')
plt.plot(x, y, 'r')
plt.xlabel('Independent Variable')
plt.ylabel('Dependent Variable')
plt.show()
# %%


"""Logarithmic"""
# %%
x = np.arange(-5.0, 5.0, 0.1)
y = np.log(x)
y_noise = 3 * np.random.normal(size=x.__len__())
ydata = y_noise + y

plt.plot(x, ydata, 'bo')
plt.plot(x, y, 'r')
plt.xlabel('Independent Variable')
plt.ylabel('Dependent Variable')
plt.show()
# %%


"""Logistic"""
# %%
x = np.arange(-5.0, 5.0, 0.1)
y = 1 - 4 / (1 + np.power(3, x - 2))
y_noise = 2 * np.random.normal(size=x.__len__())
ydata = y_noise + y

plt.plot(x, ydata, 'bo')
plt.plot(x, y, 'r')
plt.xlabel('Independent Variable')
plt.ylabel('Dependent Variable')
plt.show()
# %%


"""Non-Linear Regression"""
# %%
df = pd.read_csv('CSVs/china_gdp.csv')

# Plot dataset
plt.figure(figsize=(8, 5))
x_data, y_data = (df["Year"].values, df["Value"].values)
plt.plot(x_data, y_data, 'ro')
plt.xlabel('GDP')
plt.ylabel('Year')
plt.show()
# %%


"""Choosing a model"""
# %%
x = np.arange(-5.0, 5.0, 0.1)
y = 1.0 / (1.0 + np.exp(-x))

plt.plot(x, y)
plt.xlabel('Dependent Variable')
plt.ylabel('Independent Variable')
plt.show()
# %%


"""Build the Model"""


# %%
def sigmoid(x, Beta_1, Beta_2):
    y = 1 / (1 + np.exp(-Beta_1 * (x - Beta_2)))
    return y


beta_1 = 0.10
beta_2 = 1990.0

# Logistic function
Y_pred = sigmoid(x_data, beta_1, beta_2)

# plot initial prediction agaisnt datapoints
plt.plot(x_data, Y_pred * 15000000000000.)
plt.plot(x_data, y_data, 'ro')

# Find the best parameters for model
# normalize x and y
xdata = x_data / max(x_data)
ydata = y_data / max(y_data)
# %%

"""Find the best parameters for the fit line"""
# Use curve_fit which uses non-linear least squares to fit sigmoid function.
# %%
from scipy.optimize import curve_fit

# popt are the optimized parameters. Curve_fit fits a straight line to the dataset using function
# pcov = covariance matrix
popt, pcov = curve_fit(sigmoid, xdata, ydata)
# print the final parameters
print("beta_1 = %f, beta_2 = %f" % (popt[0], popt[1]))

# Can use this to predict new values
x_new = [1, 2, 3]
a, b = popt
y_new = sigmoid(x_new, a, b)
# y_new are the new values that are predicted
# %%

"""Plot resulting regression model"""
# %%
x = np.linspace(1960, 2015, 55)
x = x / max(x)
plt.figure(figsize=(8, 5))
y = sigmoid(x, *popt)
# %%


"""Practice"""
# %%
from sklearn.metrics import r2_score

msk = np.random.rand(len(df)) < 0.8
train_x = xdata[msk]
train_y = ydata[msk]

test_x = xdata[~msk]
test_y = ydata[~msk]

# build model using train set
popt, _ = curve_fit(sigmoid, train_x, train_y)

# predict data
a, b = popt
y_new = sigmoid(test_x, a, b)

print('%.2f' % r2_score(test_y, y_new))
# %%
