import pandas as pd
import pylab as pl
import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt
from sklearn import preprocessing

pd.set_option('display.width', 400)
pd.set_option('display.max_columns', 100)

"""
Problem: A telecommunications company is concerned about the number of customers leaving their land-line business for
cable competitors. Imagine that you are an analyst at this company and you have to find out who is leaving and why.
"""

# Linear regression used to estimate continuous values.
# Not the best for predicting observed data point
# Logistic regression estimates the class of a data point
# Logistic regression guides on what would be the most probably class for the data point

# each row represents one customer. Predict on who will stay in company.

df = pd.read_csv('CSVs/ChurnData.csv')
df = df[['tenure', 'age', 'address', 'income', 'ed', 'employ', 'equip', 'callcard', 'wireless', 'churn']]
df['churn'] = df['churn'].astype('int')

X = np.asarray(df[['tenure', 'age', 'address', 'income', 'ed', 'employ', 'equip']])
y = np.asarray(df['churn'])

# Normalize dataset
X = preprocessing.StandardScaler().fit(X).transform(X)

"""Train/Test dataset"""
# %%
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=4)
print('Train set:', X_train.shape, y_train.shape)
print('Test set:', X_test.shape, y_test.shape)
# %%


"""Modeling"""
# %%
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix

# C indicates inverse of regularization strength which must be positive float. Smaller values = stronger regularization
LR = LogisticRegression(C=0.01, solver='liblinear').fit(X_train, y_train)
yhat = LR.predict(X_test)
yhat_proba = LR.predict_proba(X_test)
# %%

"""Evaluation"""
# %%
from sklearn.metrics import jaccard_score

# 70% accuracy 
jaccard_score(y_test, yhat, pos_label=0)
# %%
