import pandas as pd
import numpy as np
import os
import matplotlib.pyplot as plt
import operator

pd.set_option('display.width', 400)
pd.set_option('display.max_columns', 15)

# %%
df = pd.read_csv('D:\\Coding\\Python\\LearningConda\\Pandas\\Sales_Data\\Sales_April_2019.csv')
files = [file for file in os.listdir('D:\\Coding\\Python\\LearningConda\\Pandas\\Sales_Data\\')]
all_months_data = pd.DataFrame()

# concatenate files into one csv
for file in files:
    # same thing as the one commented out

    # all_months_data = all_months_data.append(pd.read_csv(
    #     'D:\\Coding\\Python\\LearningConda\\Pandas\\Sales_Data\\{}'.format(file)))

    df = pd.read_csv('D:\\Coding\\Python\\LearningConda\\Pandas\\Sales_Data\\' + file)
    all_months_data = pd.concat([all_months_data, df])
    # drops the row if all the values are NaN. "Any" will drop it if it has 1 NaN
    all_months_data = all_months_data.dropna(how='all')

# Compares the list and sees if it's equal
columnList = all_months_data.columns.to_list()

# for index, row in all_months_data.iterrows():
#     if row[columnList].to_list() == columnList:
#         all_months_data = all_months_data.drop(index)

# Cleans the data by removing all data where rows were same as columns
all_months_data = all_months_data[all_months_data["Order ID"] != "Order ID"]
# %%


"""Convert Every Column to an Int"""
# %%
all_months_data['Quantity Ordered'] = pd.to_numeric(all_months_data['Quantity Ordered'], downcast='integer')
all_months_data['Order ID'] = pd.to_numeric(all_months_data['Order ID'], downcast='integer')
all_months_data['Price Each'] = pd.to_numeric(all_months_data['Price Each'])
# %%


"""Add a month column"""


# %%
def split_string(string):
    new_string = string[0:2]
    return int(new_string)


all_months_data['Month'] = all_months_data['Order Date'].apply(split_string)
# Can also do
# all_months_data['Month'] = all_months_data['Order Date'].str[0:2]

"""Adds a Sales Column"""
all_months_data['Sales'] = all_months_data['Quantity Ordered'] * all_months_data['Price Each']

# Groups data by month and returns the sales

# %%


"""Plot the Data"""
# %%
salesPerMonth = all_months_data.groupby(by='Month').sum()

# plots the data
plt.figure(figsize=(10, 5))
months = ["Jan.", "Feb.", "March", "April", "May", "June", "July", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."]
plt.title("Sales per Month in Millions")
plt.xlabel('Month')
plt.ylabel('Sales in Millions')

plt.bar(months, salesPerMonth['Sales'])
plt.yticks([0, 1000000, 2000000, 3000000, 4000000, 5000000])
# %%


"""Changes it so that the number goes to a word for the month using the above dictionary."""
# %%
months = {
    "01": "January",
    "02": "February",
    "03": "March",
    "04": "April",
    "05": "May",
    "06": "June",
    "07": "July",
    "08": "August",
    "09": "September",
    "10": "October",
    "11": "November",
    "12": "December",
}

# all_months_data['Month'] = [months[x] for x in all_months_data['Month']]
# %%


"""Add City and State Column"""


# %%
# for every x value, you want to split it by the comma and grab the first index
# Once you split it by comma, you get a list with 3 things. (['917 1st St', ' Dallas', ' TX 75001'])
# This garbs the thing in index 1

# Gets the state
def get_state(address):
    return address.split(',')[2].split(' ')[1]


def get_city(address):
    return address.split(',')[1]


all_months_data['City'] = all_months_data['Purchase Address'].apply(lambda x: get_city(x).strip() + ', ' + get_state(x))
highestSales = all_months_data.groupby(by="City").sum()

# gets unique values in a specific column
# Has to be in the same order as the Sales
cities = [city for city, df in all_months_data.groupby('City')]

plt.bar(cities, highestSales['Sales'])
plt.tight_layout()

plt.xticks(cities, rotation='vertical', size=8)
plt.xlabel('City Name')
plt.ylabel('Sales in USD ')
# %%


"""Products most often sold together"""
# %%
repeats = {}
for item in all_months_data['Order ID']:
    if item in repeats:
        repeats[item] += 1
    else:
        repeats[item] = 1

# Gets the highest value key from the dictionary
maxValueKey = max(repeats.items(), key=operator.itemgetter(1))[0]
print(all_months_data.loc[all_months_data['Order ID'] == maxValueKey])
# %%


"""What product sold the most"""
# %%
# sorts it from high -> low
all_months_data.sort_values('Quantity Ordered', ascending=False).head()

product_group = all_months_data.groupby(by='Product')
quantity_ordered = product_group.sum()['Quantity Ordered']

products = [product for product, df in product_group]

# plots the data
plt.bar(products, quantity_ordered)
plt.ylabel('Quantity Ordered')
plt.xticks(products, rotation='vertical', size=8)

# adds second y axis label
prices = all_months_data.groupby(by='Product').mean()['Price Each']
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
ax1.bar(products, quantity_ordered, color='g')
ax2.plot(products, prices, 'b-')

ax1.set_xlabel('Product Name')
ax1.set_ylabel('Quantity Ordered', color='g')
ax2.set_ylabel('Price ($)', color='b')

# how to set xticks for subplots
ax1.set_xticklabels(products, rotation='vertical', size=8)
plt.tight_layout()
# %%
