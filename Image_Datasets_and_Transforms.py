import torch
import matplotlib.pyplot as plt
import numpy as np
from torch.utils.data import Dataset, DataLoader
from matplotlib.pyplot import imshow
import matplotlib.pylab as plt
from PIL import Image
import pandas as pd
import os

# forces the random function to give same number every time we try and recompile it
torch.manual_seed(0)


# Main function to use
def show_data(data_sample, shape=(28, 28)):
    plt.imshow(data_sample[0].numpy().reshape(shape), cmap='gray')
    plt.title('y = ' + data_sample[1])


"""Auxiliary Functions"""
# %%
directory = ""
csv_file = "csvs/index.csv"
csv_path = os.path.join(directory, csv_file)

df = pd.read_csv(csv_path)

# %%


"""Load Images"""
# %%
# Combine the directory path with file name
image_name = df.iloc[1, 1]
image_path = os.path.join(directory, image_name)

image = Image.open(image_path)
plt.imshow(image, cmap='gray', vmin=0, vmax=225)
plt.title(df.iloc[1, 0])
plt.show()

# Plot the 20th image
image_path20 = df.iloc[19, 1]
image20 = Image.open(image_path20)
plt.imshow(image, cmap='gray', vmin=0, vmax=225)
plt.title(df.iloc[19, 0])
plt.show()
# %%


"""Create a Dataset Class"""


# %%
# Create dataset object

class Dataset(Dataset):

    # Constructor
    def __init__(self, csv_file, data_dir, transform=None):
        # Image directory
        self.data_dir = data_dir

        # The transform is going to be used on image
        self.transform = transform
        data_dircsv_file = os.path.join(self.data_dir, csv_file)

        # Load the csv file contains image info
        self.data_name = pd.read_csv(data_dircsv_file)

        # Number of images in dataset
        self.len = self.data_name.shape[0]

    # Get the length
    def __len__(self):
        return self.len

    # Getter
    def __getitem__(self, idx):
        # Image file path
        img_name = os.path.join(self.data_dir, self.data_name.iloc[idx, 1])

        # Open image file
        image = Image.open(img_name)

        # The class label for the image
        y = self.data_name.iloc[idx, 0]

        # If there is any transform method, apply it onto the image
        if self.transform:
            image = self.transform(image)

        # returns image text and the name of the image
        return image, y


# Create dataset objects
dataset = Dataset(csv_file=csv_file, data_dir=directory)

image = dataset[0][0]
y = dataset[0][1]

plt.imshow(image, cmap='gray', vmin=0, vmax=225)
plt.title(y)
plt.show()
# %%


"""Torchvision Transforms"""
# %%
import torchvision.transforms as transforms

# Combine two transforms: crop and convert to tensor. Apply the compose to MNIST dataset
croptensor_data_transform = transforms.Compose([transforms.CenterCrop(20), transforms.ToTensor()])
dataset = Dataset(csv_file=csv_file, data_dir=directory, transform=croptensor_data_transform)
print('The shape of the first element tensor: ', dataset[0][0].shape)

# Plot the first element in the dataset
show_data(dataset[0], shape=(20, 20))

# Vertically flip the image, and then convert it into a tensor.
# Use transforms.Compose() to combine these two transform functions
# Construct the compose. Apply it on the MNIST dataset. Plot the image
fliptensor_data_transform = transforms.Compose([transforms.RandomVerticalFlip(p=1), transforms.ToTensor()])
dataset = Dataset(csv_file=csv_file, data_dir=directory, transform=fliptensor_data_transform)
show_data(dataset[1])
# %%


"""Practice"""
# %%
# Flips it randomly horizontally.
HorizontalFliptensor_data_transform = transforms.Compose([transforms.RandomHorizontalFlip(p=1), transforms.ToTensor()])
dataset = Dataset(csv_file=csv_file, data_dir=directory, transform=HorizontalFliptensor_data_transform)
show_data(dataset[1])
# %%
