import string

alphabet = " " + string.ascii_lowercase
stringDict = {}

for positions, letter in enumerate(alphabet):
    stringDict[letter] = positions

message = "hi my name is caesar"
encodedMessage = ""

stringDictReverse = {v: k for k, v in stringDict.items()}

# encoding message
for letter in message:
    index = stringDict[letter]
    encodedMessage += stringDictReverse[index + 1]


# %%
def encoding(message, key: int):
    returnedMessage = ""

    for letter in message:
        index = stringDict[letter]
        returnedMessage += stringDictReverse[(index + key) % 27]

    return returnedMessage


# %%

encoding(encodedMessage, 3)
