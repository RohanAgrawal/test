import matplotlib.pyplot as plt
import pandas as pd
import pylab as pl
import numpy as np

pd.set_option('display.width', 400)
pd.set_option('display.max_columns', 15)

# %%
df = pd.read_csv('CSVs/FuelConsumptionCo2.csv')
df.describe()

cdf = df[['ENGINESIZE', 'CYLINDERS', 'FUELCONSUMPTION_COMB', 'CO2EMISSIONS']]
viz = cdf[['CYLINDERS', 'ENGINESIZE', 'CO2EMISSIONS', 'FUELCONSUMPTION_COMB']]
viz.hist()
plt.show()

plt.scatter(cdf['FUELCONSUMPTION_COMB'], cdf['CO2EMISSIONS'], color='blue')
plt.xlabel('Engine size')
plt.ylabel('Emission')
plt.show()

# %%


"""Plot cylinders and emissions"""
# %%
plt.scatter(cdf['CYLINDERS'], cdf['CO2EMISSIONS'], color='red')
plt.xlabel('Cylinders')
plt.ylabel('Emissions')

plt.show()
# %%


""""Creating train and test data"""
# %%
# Creates 1067 values from normal distribution (mean of 0 and std of 1)
# & writes True/False if they are less than 0.8
msk = np.random.rand(len(df)) < 0.8
# Only shows the rows where the mask value is True. If the first value of msk is True and the
# second value is False, the train data will only have that first value and leave out second value.
train = cdf[msk]
# Test data is the entire thing without the mask.
test = cdf[~msk]
# %%


"""Simple Regression Model"""
# %%
plt.scatter(train.ENGINESIZE, train.CO2EMISSIONS, color='blue')
plt.xlabel('Engine Size')
plt.ylabel('Emission')
plt.show()
# %%

"""Modeling"""
# %%
from sklearn import linear_model

regr = linear_model.LinearRegression()

# converts input to an ndarray
train_x = np.asanyarray(train[['ENGINESIZE']])
train_y = np.asanyarray(train[['CO2EMISSIONS']])
# Fits the linear model to it. X is training data and Y is target values (with mask)
regr.fit(train_x, train_y)

# The coefficients. This is the slope and the intercept
print('Coefficients: ', regr.coef_)
print('Intercept: ', regr.intercept_)

# Plot outputs
plt.scatter(train.ENGINESIZE, train.CO2EMISSIONS, color='blue')
# plots in form of x, and y (which is mx + b)
# Need to do [0][0] because default is array([[38.7435708]]) with two brackets. Other [0] only has one
plt.plot(train_x, regr.coef_[0][0] * train_x + regr.intercept_[0], '-r')
plt.xlabel("Engine size")
plt.ylabel("Emission")
# %%


"""Evaluation"""
# Compare the two and see how accurate the predicted values are
# Use MSE to calculate the accuracy of model based on test set

# Mean Absolute Error: Mean of the absolute value of the errors.
# MSE: mean of the squared error. Geared more towards large errors.
# R-squared is a metric to represent how close the data points are fitted to the regression line.
# %%
from sklearn.metrics import r2_score

test_x = np.asanyarray(test[['ENGINESIZE']])
test_y = np.asanyarray(test[['CO2EMISSIONS']])
# Predicts the x value using the linear model using the x data.
test_y_ = regr.predict(test_x)

# Compares new and old y value to see how accurate it is.
print("Mean absolute error: %.2f" % np.mean(np.absolute(test_y_ - test_y)))
print("Residual sum of squares (MSE): %.2f" % np.mean((test_y_ - test_y) ** 2))
print("R2-score: %.2f" % r2_score(test_y, test_y_))
# %%

